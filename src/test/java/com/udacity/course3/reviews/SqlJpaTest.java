package com.udacity.course3.reviews;

import com.udacity.course3.reviews.domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import javax.transaction.Transactional;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
public class SqlJpaTest {

    @Autowired private ProductRepository productRepository;
    @Autowired private ReviewRepository reviewRepository;
    @Autowired private CommentRepository commentRepository;
    @Autowired @MockBean MongoReviewRepository mongoReviewRepository;
    @Autowired @MockBean MongoCommentRepository mongoCommentRepository;

    @Test
    public void contextLoads() {
        assertThat(productRepository).isNotNull();
        assertThat(reviewRepository).isNotNull();
        assertThat(commentRepository).isNotNull();
    }

    @Test
    public void saveAndRetrieve() {

        //Insert a new Product
        Product newProduct = new Product();
        newProduct.setName("New Product");
        newProduct.setDescription("Nice New Product");
        productRepository.save(newProduct);

        //Find newly inserted product
        Optional<Product> optionalProduct1 = productRepository.findProductByName("New Product");
        Optional<Product> optionalProduct2 = productRepository.findById(newProduct.getProductId());
        Product foundProduct1 = optionalProduct1.get();
        Product foundProduct2 = optionalProduct2.get();
        System.err.println("Product: " + foundProduct1);
        assertEquals(foundProduct1.getProductId(), newProduct.getProductId());
        assertEquals(foundProduct2.getProductId(), newProduct.getProductId());

        //Create new review and insert.
        //Ensure review relates to previously inserted product.
        Review newReview = new Review();
        newReview.setText("Great Product.");
        newReview.setTitle("Wonderful Purchase");
        newReview.setProduct(newProduct);
        reviewRepository.save(newReview);

        //Find newly inserted review.
        Optional<Review> optionalReview1  = reviewRepository.findReviewByTitle("Wonderful Purchase");
        Optional<Review> optionalReview2  = reviewRepository.findById(newReview.getReviewId());
        Review foundReview1 = optionalReview1.get();
        Review foundReview2 = optionalReview2.get();
        System.err.println("Review: " + foundReview1);
        assertEquals(foundReview1.getReviewId(), newReview.getReviewId());
        assertEquals(foundReview2.getReviewId(), newReview.getReviewId());

        //Create new comment and insert.
        //Ensure comment relates to previously inserted review.
        Comment newComment = new Comment();
        newComment.setText("I agree about the new product.");
        newComment.setReview(newReview);
        commentRepository.save(newComment);

        //Find newly inserted comment.
        Optional<Comment> optionalComment1 = commentRepository.findCommentByText("I agree about the new product.");
        Optional<Comment> optionalComment2 = commentRepository.findById(newComment.getCommentId());
        Comment foundComment1 = optionalComment1.get();
        Comment foundComment2 = optionalComment2.get();
        System.err.println("Comment: " + foundComment1);
        assertEquals(foundComment1.getCommentId(),newComment.getCommentId());
        assertEquals(foundComment2.getCommentId(),newComment.getCommentId());

        //Test findAll() methods for products and any linked reviews and their linked comments.
        for(Product product: productRepository.findAll()) {

            System.err.println("\nProduct: " + product.getName() + " : " + product.getDescription());

            for (Review review : reviewRepository.findAllByProduct(product).get()) {

                System.err.println("\n\t" + review.getTitle() + " : " + review.getText());

                for (Comment comment : commentRepository.findAllByReview(review).get()) {

                    System.err.println("\n\t\t\t" + comment.getText());
                }
            }
        }
    }
}
