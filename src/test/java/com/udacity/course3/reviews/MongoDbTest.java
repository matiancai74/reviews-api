package com.udacity.course3.reviews;

import com.udacity.course3.reviews.domain.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = ReviewsApplication.class)
@DataMongoTest
@ExtendWith(SpringExtension.class)
@DirtiesContext
public class MongoDbTest {

    @Autowired @MockBean ProductRepository productRepository;
    @Autowired @MockBean ReviewRepository reviewRepository;
    @Autowired @MockBean CommentRepository commentRepository;

    @DisplayName("Checking reviews and comments are saved successfully.")
    @Test
    public void test(@Autowired MongoTemplate mongoTemplate){
        DBObject reviewToSave = BasicDBObjectBuilder.start().add("reviewId",1)
                .add("title","Great Product")
                .add("text","Very nice product review.")
                .add("productId",1)
                .get();

        mongoTemplate.save(reviewToSave,"reviews");

        assertThat(mongoTemplate.findAll(DBObject.class,"reviews")).extracting("reviewId").contains(1);
        assertThat(mongoTemplate.findAll(DBObject.class,"reviews")).extracting("title").contains("Great Product");
        assertThat(mongoTemplate.findAll(DBObject.class,"reviews")).extracting("text").contains("Very nice product review.");
        assertThat(mongoTemplate.findAll(DBObject.class,"reviews")).extracting("productId").contains(1);

        DBObject commentToSave = BasicDBObjectBuilder.start().add("commentId",1)
                .add("reviewId",1)
                .add("commentId",1)
                .add("text","Very nice product comment.")
                .get();

        mongoTemplate.save(commentToSave,"comments");

        assertThat(mongoTemplate.findAll(DBObject.class,"comments")).extracting("reviewId").contains(1);
        assertThat(mongoTemplate.findAll(DBObject.class,"comments")).extracting("commentId").contains(1);
        assertThat(mongoTemplate.findAll(DBObject.class,"comments")).extracting("text").contains("Very nice product comment.");
    }
}
