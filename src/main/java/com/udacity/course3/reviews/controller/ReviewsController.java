package com.udacity.course3.reviews.controller;

import com.udacity.course3.reviews.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Spring REST controller for working with review entity.
 */
@RestController
public class ReviewsController {

    // TODO: Wire JPA repositories here
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private MongoReviewRepository mongoReviewRepository;

    private PersistenceService persistenceService = new PersistenceService();

    /**
     * Creates a review for a product.
     * <p>
     * 1. Add argument for review entity. Use {@link RequestBody} annotation.
     * 2. Check for existence of product.
     * 3. If product not found, return NOT_FOUND.
     * 4. If found, save review.
     *
     * @param productId The id of the product.
     * @return The created review or 404 if product id is not found.
     */
    @RequestMapping(value = "/reviews/products/{productId}", method = RequestMethod.POST)
    public ResponseEntity<?> createReviewForProduct(@PathVariable("productId") Integer productId, @RequestBody Review newReview) {

        boolean successfullySaved = persistenceService.saveReview(productId, productRepository,reviewRepository,newReview);
        if(successfullySaved){
            return new ResponseEntity<Review>(newReview, HttpStatus.CREATED);
        }else{
            return new ResponseEntity<Review>(new Review(), HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Creates a MONGODB review for a product.
     * <p>
     * 1. Add argument for review entity. Use {@link RequestBody} annotation.
     * 2. Check for existence of product.
     * 3. If product not found, return NOT_FOUND.
     * 4. If found, save review.
     *
     * @param productId The id of the product.
     * @return The created review or 404 if product id is not found.
     */
    @RequestMapping(value = "/mongoreviews/products/{productId}", method = RequestMethod.POST)
    public ResponseEntity<?> createMongoReviewForProduct(@PathVariable("productId") Integer productId, @RequestBody MongoReview newReview) {
        boolean successfullySaved = persistenceService.saveReview(productId, productRepository, mongoReviewRepository,newReview);
        if(successfullySaved){
            return new ResponseEntity<MongoReview>(newReview, HttpStatus.CREATED);
        }else{
            return new ResponseEntity<Review>(new Review(), HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Lists reviews by product.
     *
     * @param productId The id of the product.
     * @return The list of reviews.
     */
    @RequestMapping(value = "/reviews/products/{productId}", method = RequestMethod.GET)
    public ResponseEntity<List<Review>> listReviewsForProduct(@PathVariable("productId") Integer productId) {
        Product product = null;
        List<Review> reviews = persistenceService.getReviewList(productId, productRepository,reviewRepository);
        if(!reviews.isEmpty()){
            return new ResponseEntity<List<Review>>(reviews, HttpStatus.OK);
        }else{
            return new ResponseEntity<List<Review>>(new ArrayList<Review>(), HttpStatus.NO_CONTENT);
        }
    }

    /**
     * Lists MONGODB reviews by product.
     *
     * @param productId The id of the product.
     * @return The list of reviews, retrived from a MONGODB collection.
     */
    @RequestMapping(value = "/mongoreviews/products/{productId}", method = RequestMethod.GET)
    public ResponseEntity<List<MongoReview>> listMongoReviewsForProduct(@PathVariable("productId") Integer productId) {
        List<MongoReview> reviewList = persistenceService.getReviewList(productId, productRepository,mongoReviewRepository);
        if(!reviewList.isEmpty()){
            return new ResponseEntity<List<MongoReview>>(reviewList, HttpStatus.OK);
        }else{
            return new ResponseEntity<List<MongoReview>>(new ArrayList<MongoReview>(), HttpStatus.NO_CONTENT);
        }
    }
}