package com.udacity.course3.reviews.controller;

import com.udacity.course3.reviews.domain.*;
import java.util.List;
import java.util.Optional;

public class PersistenceService {

    public PersistenceService(){}

    public List<Review> getReviewList(Integer productId, ProductRepository productRepository, ReviewRepository reviewRepository){
        Product product = null;
        Optional<List<Review>> reviewList = null;
        Optional<Product> optionalProduct = productRepository.findById(productId);

        if(optionalProduct.isPresent()){
            product = optionalProduct.get();
            reviewList = reviewRepository.findAllByProduct(product);
            List<Review> reviews = reviewList.get();
            if(reviews.size() > 0){
                return reviews;
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public List<MongoReview> getReviewList(Integer productId, ProductRepository productRepository, MongoReviewRepository mongoReviewRepository){

        List<MongoReview> reviewList = null;
        Optional<Product> optionalProduct = productRepository.findById(productId);

        if(optionalProduct.isPresent()){
            reviewList = mongoReviewRepository.findAllByProductId(productId);
            if(reviewList.size() > 0){
                return reviewList;
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public boolean saveReview(Integer productId, ProductRepository productRepository, ReviewRepository reviewRepository, Review newReview){
        Optional<Product> foundProduct = productRepository.findById(productId);
        Optional<Review> optionalReview = null;

        if(foundProduct.isPresent()){
            newReview.setProduct(foundProduct.get());
            reviewRepository.save(newReview);
            return true;
        }else{
            return false;
        }
    }

    public boolean saveReview(Integer productId, ProductRepository productRepository, MongoReviewRepository mongoReviewRepository, MongoReview newReview){
        Optional<Product> foundProduct = productRepository.findById(productId);
        Optional<MongoReview> optionalMongoReview = null;

        if(foundProduct.isPresent()){
            newReview.setProductId(foundProduct.get().getProductId());
            mongoReviewRepository.save(newReview);
            return true;
        }else{
            return false;
        }
    }

    public List<Comment> getCommentsList(int reviewId, ReviewRepository reviewRepository, CommentRepository commentRepository){
        Review review = null;
        Optional<List<Comment>> commentList = null;
        Optional<Review> optionalReview = reviewRepository.findById(reviewId);

        if(optionalReview.isPresent()){
            review = optionalReview.get();
            return commentRepository.findAllByReview(review).get();
        }else{
            return null;
        }
    }

    public List<MongoComment> getCommentsList(int reviewId, MongoReviewRepository mongoReviewRepository){
        MongoReview review = null;
        List<MongoComment> commentList = null;
        Optional<MongoReview> optionalReview = mongoReviewRepository.findMongoReviewByReviewId(reviewId);

        if(optionalReview.isPresent()){
            return optionalReview.get().getComments();
        }else{
            return null;
        }
    }

    public Comment saveComment(int reviewId, Comment newComment, ReviewRepository reviewRepository, CommentRepository commentRepository){
        Optional<Review> foundReview = reviewRepository.findById(reviewId);
        Optional<Comment> optionalComment = null;

        if(foundReview.isPresent() &&(!(foundReview == null))){
            newComment.setReview(foundReview.get());
            commentRepository.save(newComment);
            return newComment;
        }else{
            return null;
        }
    }

    public MongoComment saveComment(int reviewId, MongoComment newComment, MongoReviewRepository mongoReviewRepository){

        Optional<MongoReview> foundReview = mongoReviewRepository.findMongoReviewByReviewId(reviewId);

        if(foundReview.isPresent()){
            MongoReview review = foundReview.get();
            newComment.setCommentId(review.getComments().size()+1);
            newComment.setReviewId(reviewId);
            review.addCommentToList(newComment);
            mongoReviewRepository.deleteByReviewId(reviewId);
            mongoReviewRepository.save(review);
            return newComment;
        }else{
            return null;
        }

    }
}