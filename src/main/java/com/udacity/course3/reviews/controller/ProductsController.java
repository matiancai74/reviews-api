package com.udacity.course3.reviews.controller;

import com.udacity.course3.reviews.domain.CommentRepository;
import com.udacity.course3.reviews.domain.Product;
import com.udacity.course3.reviews.domain.ProductRepository;
import com.udacity.course3.reviews.domain.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

/**
 * Spring REST controller for working with product entity.
 */
@RestController
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    private ProductRepository productRepository;

    /**
     * Creates a product.
     *
     * 1. Accept product as argument. Use {@link RequestBody} annotation.
     * 2. Save product.
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> createProduct(@RequestBody Product product) {

        productRepository.save(product);
        Optional<Product> optionalProduct = productRepository.findById(product.getProductId());

        if(optionalProduct.isPresent()){
            return new ResponseEntity<Product>(optionalProduct.get(), HttpStatus.CREATED);
        }else{
            return new ResponseEntity<Product>(new Product(), HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds a product by id.
     *
     * @param id The id of the product.
     * @return The product if found, or a 404 not found.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Product> findById(@PathVariable("id") Integer id) {

        Optional<Product> optionalProduct = productRepository.findProductByProductId(id);

        if(optionalProduct.isPresent()){
            return new ResponseEntity<Product>(optionalProduct.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<Product>(new Product(), HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Lists all products.
     *
     * @return The list of products.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Product> listProducts() {
        return productRepository.findAll();
    }
}