package com.udacity.course3.reviews.controller;

import com.udacity.course3.reviews.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Spring REST controller for working with comment entity.
 */
@RestController
@RequestMapping("/comments")
public class CommentsController {

    // TODO: Wire needed JPA repositories here

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private MongoReviewRepository mongoReviewRepository;

    private PersistenceService persistenceService = new PersistenceService();

    /**
     * Creates a comment for a review.
     *
     * 1. Add argument for comment entity. Use {@link RequestBody} annotation.
     * 2. Check for existence of review.
     * 3. If review not found, return NOT_FOUND.
     * 4. If found, save comment.
     *
     * @param reviewId The id of the review.
     */
    @RequestMapping(value = "/comments/{reviewId}", method = RequestMethod.POST)
    public ResponseEntity<?> saveComment(@PathVariable("reviewId") Integer reviewId, @RequestBody Comment newComment) {

       Comment comment = persistenceService.saveComment(reviewId,newComment,reviewRepository,commentRepository);
       if(!(comment == null)){
           return new ResponseEntity<Comment>((comment), HttpStatus.CREATED);
       }else{
           return new ResponseEntity<Comment>(new Comment(), HttpStatus.NOT_FOUND);
       }
    }

    @RequestMapping(value = "/mongocomments/{reviewId}", method = RequestMethod.POST)
    public ResponseEntity<?> saveMongoComment(@PathVariable("reviewId") Integer reviewId, @RequestBody MongoComment newComment) {

        MongoComment comment = persistenceService.saveComment(reviewId, newComment, mongoReviewRepository);
        if(!(comment == null)){
            return new ResponseEntity<MongoComment>((comment), HttpStatus.CREATED);
        }else{
            return new ResponseEntity<MongoComment>(new MongoComment(), HttpStatus.NOT_FOUND);
        }
    }

    /**
     * List comments for a review.
     *
     * 2. Check for existence of review.
     * 3. If review not found, return NOT_FOUND.
     * 4. If found, return list of comments.
     *
     * @param reviewId The id of the review.
     */
    @RequestMapping(value = "/comments/{reviewId}", method = RequestMethod.GET)
    public ResponseEntity<List<Comment>> listCommentsForReview(@PathVariable("reviewId") Integer reviewId) {

        List<Comment> commentList = persistenceService.getCommentsList(reviewId, reviewRepository, commentRepository);
        if(!commentList.isEmpty() && !(commentList == null)){
            return new ResponseEntity<List<Comment>>(commentList, HttpStatus.OK);
        }else{
            return new ResponseEntity<List<Comment>>(new ArrayList<Comment>(), HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/mongocomments/{reviewId}", method = RequestMethod.GET)
    public ResponseEntity<List<MongoComment>> listMongoCommentsForReview(@PathVariable("reviewId") Integer reviewId) {

        List<MongoComment> commentList = persistenceService.getCommentsList(reviewId, mongoReviewRepository);
        if(!commentList.isEmpty() && !(commentList == null)){
            return new ResponseEntity<List<MongoComment>>(commentList, HttpStatus.OK);
        }else{
            return new ResponseEntity<List<MongoComment>>(new ArrayList<MongoComment>(), HttpStatus.NO_CONTENT);
        }
    }
}