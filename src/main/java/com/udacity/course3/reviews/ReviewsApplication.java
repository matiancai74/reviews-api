package com.udacity.course3.reviews;

import org.flywaydb.core.Flyway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.udacity.course3.reviews.domain")
@EnableMongoRepositories(basePackages = "com.udacity.course3.reviews")
public class ReviewsApplication {
	public static void main(String[] args) {
		SpringApplication.run(ReviewsApplication.class, args);
		Flyway flyway = Flyway.configure().dataSource("jdbc:mysql://localhost/newreviews?serverTimezone=UTC", "matt", "matt").load();
		flyway.clean();
		flyway.migrate();
	}
}