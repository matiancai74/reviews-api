package com.udacity.course3.reviews.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {
    Optional<Product> findProductByName(String name);
    Optional<Product> findProductByProductId(Integer id);
    List<Product> findAll();

}
