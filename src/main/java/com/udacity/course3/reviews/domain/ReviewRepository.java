package com.udacity.course3.reviews.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReviewRepository extends CrudRepository<Review, Integer> {
    Optional<List<Review>> findAllByProduct(Product product);
    Optional<Review> findReviewByTitle(String title);
}
