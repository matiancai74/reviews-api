package com.udacity.course3.reviews.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer> {
    Optional<List<Comment>> findAllByReview(Review review);
    Optional<Comment> findCommentByText(String text);
}
