package com.udacity.course3.reviews.domain;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface MongoReviewRepository extends MongoRepository<MongoReview, Integer> {
    public Optional<MongoReview> findMongoReviewByReviewId(int id);
    public List<MongoReview> findAll();

    public List<MongoReview> findAllByProductId(int productId);
    public void deleteByReviewId(int reviewId);

}
