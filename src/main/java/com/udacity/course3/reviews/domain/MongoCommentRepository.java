package com.udacity.course3.reviews.domain;


import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MongoCommentRepository extends MongoRepository<MongoComment, ObjectId> {
    public List<MongoComment> findAll();
}
