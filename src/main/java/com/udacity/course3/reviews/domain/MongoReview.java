package com.udacity.course3.reviews.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Document("reviews")
public class MongoReview {

    @Id
    private Integer reviewId;

    String title;

    String text;

    private int productId;

    private List<MongoComment> comments = new ArrayList<MongoComment>();

    public MongoReview() {
    }

    public int getReviewId() {
        return reviewId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<MongoComment> getComments() {
        return comments;
    }

    public boolean addCommentToList(MongoComment mongoComment){
        if(!(mongoComment == null)){
            comments.add(mongoComment);
            return true;
        }else{
            return false;
        }
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    @Override
    public String toString() {
        return "Review{" +
                "reviewId=" + reviewId +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
