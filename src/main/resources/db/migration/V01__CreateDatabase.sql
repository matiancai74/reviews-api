CREATE TABLE `products` (
        `productid` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(100) DEFAULT NULL,
        `description` varchar(1000) DEFAULT NULL,
        PRIMARY KEY (`productid`)
);

INSERT INTO `products` VALUES (1,'Sunglasses','Cool shades.'),
                              (2,'Xbox One X','Games Console'),
                              (3,'Keyboard','Mechanical RGB LED Keyboard'),
                              (4,'Bose Headphones','Noise canceling headphones'),
                              (5,'LED Lamp','Low light lamp'),
                              (6,'Batman Teacup','Large black teacup with Batman logo.'),
                              (7,'Xbox Scarlet','2020 Games Console'),
                              (8,'Skeletron Keepsake Box','Terraria Merchandise'),
                              (9, 'Xbox One Controller','Gears 5 Controller');


CREATE TABLE `reviews` (
   `reviewid` int(11) NOT NULL AUTO_INCREMENT,
   `title` varchar(255) DEFAULT NULL,
   `text` varchar(1000) DEFAULT NULL,
   `productid` int(11) NOT NULL,
   PRIMARY KEY (`reviewid`),
   CONSTRAINT `product_fk` FOREIGN KEY (`productid`) REFERENCES `products` (`productid`)
);

INSERT INTO `reviews` VALUES (1,'Nice Shades','Best shades I ever used.',1),
                             (2,'Great, but expensive','Really good but too pricey.',1),
                             (3,'Best Games Console','Powerful, with great graphics.',2),
                             (4,'Not as good as Sony','PS4 is better.',2),
                             (5,'Excellent Keyboard','Very responsive and nice to use.',3),
                             (6,'Good Keyboard','A bit oversensitive for my liking.',3),
                             (7,'Great Headphones','Great sound and comfort.',4),
                             (8,'Great Headphones','Great sound but needs more bass.',4),
                             (9,'Average Lamp','Nice but not bright enough.',5),
                             (10,'Nice in the Living Room','Looks nice and stylish',5),
                             (11,'Best Batman cup ever!','Not the teacup we wanted, but the teacup we deserved.',6),
                             (12,'Excellent Product','My kids really love this.',8),
                             (13,'Even more powerful than the XBone!','This will be an excellent console.',7),
                             (14,'Nice Controller','Has ribbed triggers that are very responsive.',9),
                             (15,'Solid Controller','Good quality, sturdy controller for long game sessions.',9);

CREATE TABLE `comments` (
    `commentid` int(11) NOT NULL AUTO_INCREMENT,
    `text` varchar(1000) DEFAULT NULL,
    `reviewid` int(11) NOT NULL,
    PRIMARY KEY (`commentid`),
    CONSTRAINT `review_fk` FOREIGN KEY (`reviewid`) REFERENCES `reviews` (`reviewid`)
);

INSERT INTO `comments` VALUES (1,'They barely protect you from the sun, no good.',1),
                              (2,'Yeah, really good product, excellent.',1),
                              (3,'You get what you pay for.',2),
                              (4,'Really nice sunglasses eh.',2),
                              (5,'Definitely great but not as good as PC.',3),
                              (6,'You are crazy, its much more powerful than PS4.',4),
                              (7,'Sorry but you are wrong, PS4 is not as powerful.',4),
                              (8,'Sorry but you are wrong, this keyboard is awful.',5),
                              (9,'This keyboard is great, for sure.',5),
                              (10,'This keyboard definitely is oversensitive.',6),
                              (11,'Best value for money, these headphones.',7),
                              (12,'Agreed - the bass does need a bit of amplification.',8),
                              (13,'Agreed - the light is not bright enough.',9),
                              (14,'Agreed - the lamp looks nice in my room too.',10),
                              (15,'Not the teacup we wanted, but the teacup we deserved.',11),
                              (16,'That is for sure, this will be EPIC!',13),
                              (17,'Those triggers really helped me beat the game.', 14),
                              (18,'Wrong - I broke this controller the first time I used it.', 15);